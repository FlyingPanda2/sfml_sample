﻿#include <iostream>
#include <fstream>

int main()
{
	using namespace std;
	setlocale( LC_ALL, "Rus" );
	ifstream fin("input.txt");
	ofstream fout("output.txt");
	int n, m;
	cout << "Введите размеры матрицы :" << endl;
	cin >> n;
	cin >> m;
	if (n < 0 || m < 0)
	{
		cout << "Размеры матрицы не могут быть отрицательными";
		return 1;
	}
	int **matrix = new int*[n];
	for (int i = 0; i < n; i++)
	{
		matrix[i] = new int[m];
	}
	int min = INT_MAX;
	int index = 0;
	int stolb_sum = 0;
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			fin >> matrix[i][j];
		}
	}
	fin.close();
	fout << "Ваша матрица :" << endl;
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			fout << matrix[i][j] << "\t";
		}
		fout << endl;
	}
	fout << endl;
	for (int j = 0; j < m; j++)
	{
		stolb_sum = 0;
		for (int i = 0; i < n; i++)
		{
			stolb_sum += matrix[i][j];
		}
		if (stolb_sum < min)
		{
			min = stolb_sum;
			index = j;
		}
	}
	fout << endl;
	for (int i = 0; i < m; i++)
	{
		matrix[i][index] += 3;
	}
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			fout << matrix[i][j] << "\t";
		}
		fout << endl;
	}
	fout.close();
	for (int i = 0; i < n; i++)
	{
		delete[] matrix[i];    
	}
	delete[] matrix;
	return 0;
}
