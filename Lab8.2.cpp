﻿#include <iostream>

int main()
{
    setlocale(LC_ALL, "Russian");
    using namespace std;
    cout << "Введите длину последовательности" << endl;
    int n;
    cin >> n;
    if (n < 0)
    {
        cout << "Длина послеовательности не может быть отрицательной";
    }
    int *mass = new int[n];
    for (int i = 0; i < n; i++)
    {
        cout << "Введите число последовательности номер " << i + 1 << endl;
        cin >> mass[i];
    }
    for (int i = 0; i < n; i++)
    {
        for (int j = i + 1; j < n; j++)
        {
            int not_negative = mass[i];
            int not_negative2 = mass[j];
            not_negative = abs(not_negative);
            not_negative2 = abs(not_negative2);
            int num1 = not_negative;
            int num2 = not_negative2;
            while (not_negative > 0)
            {
                num1 = not_negative % 10;
                not_negative /= 10;
            }
            while (not_negative2 > 0)
            {
                num2 = not_negative2 % 10;
                not_negative2 /= 10;
            }
            if (num1 > num2)
            {
                swap(mass[i], mass[j]);
            }
            else if (num1 == num2)
            {
                int not_negative3 = mass[i];
                int not_negative4 = mass[j];
                not_negative3 = abs(not_negative3);
                not_negative4 = abs(not_negative4);
                int equal_num1 = not_negative3;
                int equal_num2 = not_negative4;
                int multiplication = 1;
                int multiplication2 = 1;
                while (not_negative3 > 0)
                {
                    equal_num1 = not_negative3 % 10;
                    multiplication = multiplication * equal_num1;
                    not_negative3 /= 10;
                }
                while (not_negative4 > 0)
                {
                    equal_num2 = not_negative4 % 10;
                    multiplication2 = multiplication2 * equal_num2;
                    not_negative4 /= 10;
                }
                if (multiplication > multiplication2)
                {
                    swap(mass[i], mass[j]);
                }
                else if (multiplication == multiplication2)
                {
                    if (mass[i] > mass[j])
                    {
                        swap(mass[i], mass[j]);
                    }
                }
            }
        }
    }
    for (int i = 0; i < n; i++)
    {
        cout << mass[i] << " ";
    }
    delete[] mass;
    return 0;
}
